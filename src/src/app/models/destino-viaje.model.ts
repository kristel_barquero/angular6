export class DestinoViaje {
    nombre:string;
    apellido: string;
    descripcion:string;
    imagenUrl:string;

    constructor(n:string, a:string, d:string, u:string){
        this.nombre = n;
        this.apellido = a;
        this.descripcion = d;
        this.imagenUrl = u;
    }
}